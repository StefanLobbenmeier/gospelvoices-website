+++
cascade = ""
featured_image = ""
title = "Probe & Aktuelles"
type = "page"
[menu.main]
weight = 2

+++
# Aktuelles

Am 10.08.2022 werden wir nach der langen Zeit endlich wieder mit unseren Proben starten. Proben-Besucher werden gebeten sich vorab telefonisch beim Vorstand telefonisch unter 02305 549085 anzumelden und sich über die getroffenen Hygienemaßnahmen zu informieren.

Wir proben jeden Mittwoch von 20-22 Uhr (probenfreie Zeiten siehe unter „Aktuell und sehr bald“)

# Adresse Probenraum

Ev. Gemeindehaus Dietrich-Bonhoeffer-Haus  
Am Weißdorn 2a  
44577 Castrop-Rauxel